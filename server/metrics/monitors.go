package metrics

import (
	"bufio"
	"fmt"
	"git.openprivacy.ca/openprivacy/libricochet-go/application"
	"github.com/struCoder/pidusage"
	"log"
	"os"
	"time"
)

const (
	reportFile = "serverMonitorReport.txt"
)

// Monitors is a package of metrics for a Cwtch Server including message count, CPU, Mem, and conns
type Monitors struct {
	MessageCounter Counter
	Messages       MonitorHistory
	CPU            MonitorHistory
	Memory         MonitorHistory
	ClientConns    MonitorHistory
	starttime      time.Time
	breakChannel   chan bool
	log            bool
}

// Start initializes a Monitors's monitors
func (mp *Monitors) Start(ra *application.RicochetApplication, log bool) {
	mp.log = log
	mp.starttime = time.Now()
	mp.breakChannel = make(chan bool)
	mp.MessageCounter = NewCounter()
	mp.Messages = NewMonitorHistory(Count, Cumulative, func() (c float64) { c = float64(mp.MessageCounter.Count()); mp.MessageCounter.Reset(); return })
	mp.CPU = NewMonitorHistory(Percent, Average, func() float64 { sysInfo, _ := pidusage.GetStat(os.Getpid()); return float64(sysInfo.CPU) })
	mp.Memory = NewMonitorHistory(MegaBytes, Average, func() float64 { sysInfo, _ := pidusage.GetStat(os.Getpid()); return float64(sysInfo.Memory) })
	mp.ClientConns = NewMonitorHistory(Count, Average, func() float64 { return float64(ra.ConnectionCount()) })

	if mp.log {
		go mp.run()
	}
}

func (mp *Monitors) run() {
	for {
		select {
		case <-time.After(time.Minute):
			mp.report()
		case <-mp.breakChannel:
			return
		}
	}
}

func (mp *Monitors) report() {
	f, err := os.Create(reportFile)
	if err != nil {
		log.Println("ERROR: Could not open monitor reporting file: ", err)
		return
	}
	defer f.Close()

	w := bufio.NewWriter(f)

	fmt.Fprintf(w, "Uptime: %v\n\n", time.Now().Sub(mp.starttime))

	fmt.Fprintln(w, "Messages:")
	mp.Messages.Report(w)

	fmt.Fprintln(w, "\nClient Connections:")
	mp.ClientConns.Report(w)

	fmt.Fprintln(w, "\nCPU:")
	mp.CPU.Report(w)

	fmt.Fprintln(w, "\nMemory:")
	mp.Memory.Report(w)

	w.Flush()
}

// Stop stops all the monitors in a Monitors
func (mp *Monitors) Stop() {
	if mp.log {
		mp.breakChannel <- true
	}
	mp.Messages.Stop()
	mp.CPU.Stop()
	mp.Memory.Stop()
	mp.ClientConns.Stop()
}
