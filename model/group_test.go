package model

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"testing"
	"time"
)

func TestGroup(t *testing.T) {
	g, _ := NewGroup("server.onion")
	dgm := &protocol.DecryptedGroupMessage{
		Onion:              proto.String("onion"),
		Text:               proto.String("Hello World!"),
		Timestamp:          proto.Int32(int32(time.Now().Unix())),
		SignedGroupId:      []byte{},
		PreviousMessageSig: []byte{},
		Padding:            []byte{},
	}
	encMessage, _ := g.EncryptMessage(dgm)
	ok, message := g.DecryptMessage(encMessage)
	if !ok || message.GetText() != "Hello World!" {
		t.Errorf("group encryption was invalid, or returned wrong message decrypted:%v message:%v", ok, message)
		return
	}
	t.Logf("Got message %v", message)
}
