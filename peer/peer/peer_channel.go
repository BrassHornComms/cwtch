package peer

import (
	"cwtch.im/cwtch/protocol"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"github.com/golang/protobuf/proto"
	"log"
)

// CwtchPeerChannel implements the ChannelHandler interface for a channel of
// type "im.ricochet.Cwtch". The channel may be inbound or outbound.
//
// CwtchPeerChannel implements protocol-level sanity and state validation, but
// does not handle or acknowledge Cwtch messages. The application must provide
// a CwtchPeerChannelHandler implementation to handle Cwtch events.
type CwtchPeerChannel struct {
	// Methods of Handler are called for Cwtch events on this channel
	Handler CwtchPeerChannelHandler
	channel *channels.Channel
}

// CwtchPeerChannelHandler is implemented by an application type to receive
// events from a CwtchPeerChannel.
type CwtchPeerChannelHandler interface {
	ClientIdentity(*protocol.CwtchIdentity)
	HandleGroupInvite(*protocol.GroupChatInvite)
	GetClientIdentityPacket() []byte
}

// SendMessage sends a raw message on this channel
func (cpc *CwtchPeerChannel) SendMessage(data []byte) {
	cpc.channel.SendMessage(data)
}

// Type returns the type string for this channel, e.g. "im.ricochet.Cwtch".
func (cpc *CwtchPeerChannel) Type() string {
	return "im.cwtch.peer"
}

// Closed is called when the channel is closed for any reason.
func (cpc *CwtchPeerChannel) Closed(err error) {

}

// OnlyClientCanOpen  - for Cwtch channels any side can open
func (cpc *CwtchPeerChannel) OnlyClientCanOpen() bool {
	return false
}

// Singleton - for Cwtch channels there can only be one instance per direction
func (cpc *CwtchPeerChannel) Singleton() bool {
	return true
}

// Bidirectional - for Cwtch channels are not bidrectional
func (cpc *CwtchPeerChannel) Bidirectional() bool {
	return false
}

// RequiresAuthentication - Cwtch channels require hidden service auth
func (cpc *CwtchPeerChannel) RequiresAuthentication() string {
	return "im.ricochet.auth.3dh"
}

// OpenInbound is the first method called for an inbound channel request.
// If an error is returned, the channel is rejected. If a RawMessage is
// returned, it will be sent as the ChannelResult message.
func (cpc *CwtchPeerChannel) OpenInbound(channel *channels.Channel, raw *Protocol_Data_Control.OpenChannel) ([]byte, error) {
	cpc.channel = channel
	messageBuilder := new(utils.MessageBuilder)
	return messageBuilder.AckOpenChannel(channel.ID), nil
}

// OpenOutbound is the first method called for an outbound channel request.
// If an error is returned, the channel is not opened. If a RawMessage is
// returned, it will be sent as the OpenChannel message.
func (cpc *CwtchPeerChannel) OpenOutbound(channel *channels.Channel) ([]byte, error) {
	cpc.channel = channel
	messageBuilder := new(utils.MessageBuilder)
	return messageBuilder.OpenChannel(channel.ID, cpc.Type()), nil
}

// OpenOutboundResult is called when a response is received for an
// outbound OpenChannel request. If `err` is non-nil, the channel was
// rejected and Closed will be called immediately afterwards. `raw`
// contains the raw protocol message including any extension data.
func (cpc *CwtchPeerChannel) OpenOutboundResult(err error, crm *Protocol_Data_Control.ChannelResult) {
	if err == nil {
		if crm.GetOpened() {
			cpc.channel.Pending = false
		}
	}
}

// Packet is called for each raw packet received on this channel.
func (cpc *CwtchPeerChannel) Packet(data []byte) {
	cpp := &protocol.CwtchPeerPacket{}
	err := proto.Unmarshal(data, cpp)
	if err == nil {
		if cpp.GetCwtchIdentify() != nil {
			cpc.Handler.ClientIdentity(cpp.GetCwtchIdentify())
			pkt := cpc.Handler.GetClientIdentityPacket()
			if pkt != nil {
				cpc.SendMessage(pkt)
			}
		} else if cpp.GetGroupChatInvite() != nil {
			cpc.Handler.HandleGroupInvite(cpp.GetGroupChatInvite())
		}
	} else {
		log.Printf("Error Receivng Packet %v\n", err)
	}
}
