package peer

import (
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
)

// CwtchPeerDataChannel implements the ChannelHandler interface for a channel of
// type "im.cwtch.peer.data The channel may be inbound or outbound.
//
// CwtchPeerChannel implements protocol-level sanity and state validation, but
// does not handle or acknowledge Cwtch messages. The application must provide
// a CwtchPeerChannelHandler implementation to handle Cwtch events.
type CwtchPeerDataChannel struct {
	// Methods of Handler are called for Cwtch events on this channel
	Handler CwtchPeerDataHandler
	channel *channels.Channel
}

// CwtchPeerDataHandler is implemented by an application type to receive
// events from a CwtchPeerChannel.
type CwtchPeerDataHandler interface {
	HandlePacket([]byte) []byte
}

// SendMessage sends a raw message on this channel
func (cpc *CwtchPeerDataChannel) SendMessage(data []byte) {
	cpc.channel.SendMessage(data)
}

// Type returns the type string for this channel, e.g. "im.ricochet.Cwtch".
func (cpc *CwtchPeerDataChannel) Type() string {
	return "im.cwtch.peer.data"
}

// Closed is called when the channel is closed for any reason.
func (cpc *CwtchPeerDataChannel) Closed(err error) {

}

// OnlyClientCanOpen  - for Cwtch channels any side can open
func (cpc *CwtchPeerDataChannel) OnlyClientCanOpen() bool {
	return false
}

// Singleton - for Cwtch channels there can only be one instance per direction
func (cpc *CwtchPeerDataChannel) Singleton() bool {
	return true
}

// Bidirectional - for Cwtch channels are bidirectional
func (cpc *CwtchPeerDataChannel) Bidirectional() bool {
	return true
}

// RequiresAuthentication - Cwtch channels require hidden service auth
func (cpc *CwtchPeerDataChannel) RequiresAuthentication() string {
	return "im.ricochet.auth.3dh"
}

// OpenInbound is the first method called for an inbound channel request.
// If an error is returned, the channel is rejected. If a RawMessage is
// returned, it will be sent as the ChannelResult message.
func (cpc *CwtchPeerDataChannel) OpenInbound(channel *channels.Channel, raw *Protocol_Data_Control.OpenChannel) ([]byte, error) {
	cpc.channel = channel
	messageBuilder := new(utils.MessageBuilder)
	return messageBuilder.AckOpenChannel(channel.ID), nil
}

// OpenOutbound is the first method called for an outbound channel request.
// If an error is returned, the channel is not opened. If a RawMessage is
// returned, it will be sent as the OpenChannel message.
func (cpc *CwtchPeerDataChannel) OpenOutbound(channel *channels.Channel) ([]byte, error) {
	cpc.channel = channel
	messageBuilder := new(utils.MessageBuilder)
	return messageBuilder.OpenChannel(channel.ID, cpc.Type()), nil
}

// OpenOutboundResult is called when a response is received for an
// outbound OpenChannel request. If `err` is non-nil, the channel was
// rejected and Closed will be called immediately afterwards. `raw`
// contains the raw protocol message including any extension data.
func (cpc *CwtchPeerDataChannel) OpenOutboundResult(err error, crm *Protocol_Data_Control.ChannelResult) {
	if err == nil {
		if crm.GetOpened() {
			cpc.channel.Pending = false
		}
	}
}

// Packet is called for each raw packet received on this channel.
func (cpc *CwtchPeerDataChannel) Packet(data []byte) {
	ret := cpc.Handler.HandlePacket(data)
	if len(ret) >= 0 {
		cpc.channel.SendMessage(ret)
	}
}
