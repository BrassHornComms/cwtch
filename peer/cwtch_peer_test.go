package peer

import (
	"testing"
)

func TestCwtchPeerGenerate(t *testing.T) {

	alice, _ := NewCwtchPeer("alice", "testpass", "./alice.json")
	alice.Save()

	aliceLoaded, err := LoadCwtchPeer("./alice.json", "testpass")
	if err != nil || aliceLoaded.GetProfile().Name != "alice" {
		t.Errorf("something went wrong saving and loading profiles %v %v", err, aliceLoaded)
	}

	groupID, _, _ := aliceLoaded.StartGroup("test.server")
	exportedGroup, _ := aliceLoaded.ExportGroup(groupID)
	t.Logf("Exported Group: %v from %v", exportedGroup, aliceLoaded.GetProfile().Onion)

	importedGroupID, err := alice.ImportGroup(exportedGroup)
	group := alice.GetGroup(importedGroupID)
	t.Logf("Imported Group: %v, err := %v %v", group, err, importedGroupID)

}

func TestTrustPeer(t *testing.T) {
	groupName := "test.server"
	alice, _ := NewCwtchPeer("alice", "alicepass", "")
	bob, _ := NewCwtchPeer("bob", "bobpass", "")

	bobOnion := bob.GetProfile().Onion
	aliceOnion := alice.GetProfile().Onion

	groupID, _, err := alice.StartGroup(groupName)
	if err != nil {
		t.Error(err)
	}

	groupAlice := alice.GetGroup(groupID)
	if groupAlice.GroupID != groupID {
		t.Errorf("Alice should be part of group %v, got %v instead", groupID, groupAlice)
	}

	exportedGroup, err := alice.ExportGroup(groupID)
	if err != nil {
		t.Error(err)
	}

	err = alice.InviteOnionToGroup(bobOnion, groupID)
	if err == nil {
		t.Errorf("onion invitation should fail since alice does no trust bob")
	}

	err = alice.TrustPeer(bobOnion)
	if err == nil {
		t.Errorf("trust peer should fail since alice does not know about bob")
	}

	// bob adds alice contact by importing serialized group created by alice
	_, err = bob.ImportGroup(exportedGroup)
	if err != nil {
		t.Error(err)
	}
	err = bob.TrustPeer(aliceOnion)
	if err != nil {
		t.Errorf("bob must be able to trust alice, got %v", err)
	}

	err = bob.InviteOnionToGroup(aliceOnion, groupID)
	if err == nil {
		t.Errorf("bob trusts alice but peer connection is not ready yet. should not be able to invite her to group, instead got: %v", err)
	}
}
