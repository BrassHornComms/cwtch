package connections

import (
	"crypto/rand"
	"cwtch.im/cwtch/model"
	"cwtch.im/cwtch/peer/peer"
	"cwtch.im/cwtch/protocol"
	"git.openprivacy.ca/openprivacy/libricochet-go"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/connection"
	"git.openprivacy.ca/openprivacy/libricochet-go/identity"
	"golang.org/x/crypto/ed25519"
	"net"
	"testing"
	"time"
)

func PeerAuthValid(hostname string, key ed25519.PublicKey) (allowed, known bool) {
	return true, true
}

func runtestpeer(t *testing.T, tp *TestPeer, identity identity.Identity) {
	ln, _ := net.Listen("tcp", "127.0.0.1:5452")
	conn, _ := ln.Accept()
	defer conn.Close()

	rc, err := goricochet.NegotiateVersionInbound(conn)
	if err != nil {
		t.Errorf("Negotiate Version Error: %v", err)
	}
	rc.TraceLog(true)
	err = connection.HandleInboundConnection(rc).ProcessAuthAsV3Server(identity, PeerAuthValid)
	if err != nil {
		t.Errorf("ServerAuth Error: %v", err)
	}
	tp.RegisterChannelHandler("im.cwtch.peer", func() channels.Handler {
		cpc := new(peer.CwtchPeerChannel)
		cpc.Handler = tp
		return cpc
	})

	go func() {
		alice := model.GenerateNewProfile("alice")
		time.Sleep(time.Second * 1)
		rc.Do(func() error {
			channel := rc.Channel("im.cwtch.peer", channels.Inbound)
			if channel != nil {
				peerchannel, ok := channel.Handler.(*peer.CwtchPeerChannel)
				if ok {
					peerchannel.SendMessage(alice.GetCwtchIdentityPacket())
				}
			}
			return nil
		})
	}()

	rc.Process(tp)
}

type TestPeer struct {
	connection.AutoConnectionHandler
	ReceivedIdentityPacket bool
	ReceivedGroupInvite    bool
}

func (tp *TestPeer) ClientIdentity(ci *protocol.CwtchIdentity) {
	tp.ReceivedIdentityPacket = true
}

func (tp *TestPeer) HandleGroupInvite(gci *protocol.GroupChatInvite) {
	tp.ReceivedGroupInvite = true
}

func (tp *TestPeer) GetClientIdentityPacket() []byte {
	return nil
}

func TestPeerPeerConnection(t *testing.T) {
	pub, priv, _ := ed25519.GenerateKey(rand.Reader)
	identity := identity.InitializeV3("", &priv, &pub)

	profile := model.GenerateNewProfile("alice")
	hostname := identity.Hostname()
	ppc := NewPeerPeerConnection("127.0.0.1:5452|"+hostname, profile, nil)

	tp := new(TestPeer)
	tp.Init()
	go runtestpeer(t, tp, identity)
	state := ppc.GetState()
	if state != DISCONNECTED {
		t.Errorf("new connections should start in disconnected state")
	}
	go ppc.Run()
	time.Sleep(time.Second * 5)
	state = ppc.GetState()
	if state != AUTHENTICATED {
		t.Errorf("connection state should be authenticated(3), was instead %v", state)
	}

	if tp.ReceivedIdentityPacket == false {
		t.Errorf("should have received an identity packet")
	}

	_, invite, _ := profile.StartGroup("aaa.onion")
	ppc.SendGroupInvite(invite)
	time.Sleep(time.Second * 3)
	if tp.ReceivedGroupInvite == false {
		t.Errorf("should have received an group invite packet")
	}

}
