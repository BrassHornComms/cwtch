package connections

// ConnectionState defines the various states a connection can be in from disconnected to authenticated
type ConnectionState int

// Connection States
// DISCONNECTED - No existing connection has been made, or all attempts have failed
// CONNECTING - We are in the process of attempting to connect to a given endpoint
// CONNECTED - We have connected but not yet authenticated
// AUTHENTICATED - im.ricochet.auth-hidden-server has succeeded on thec onnection.
const (
	DISCONNECTED ConnectionState = iota
	CONNECTING
	CONNECTED
	AUTHENTICATED
	FAILED
	KILLED
)

var (
	// ConnectionStateName allows conversaion of states to their string representations
	ConnectionStateName = []string{"Disconnected", "Connecting", "Connected", "Authenticated", "Failed", "Killed"}
)
