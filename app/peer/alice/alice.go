package main

import (
	"cwtch.im/cwtch/peer"
	"log"
)

func main() {
	alice, _ := peer.NewCwtchPeer("alice", "password", ".")

	processData := func(onion string, data []byte) []byte {
		log.Printf("Recieved %s from %v", data, onion)
		return data
	}

	alice.SetPeerDataHandler(processData)
	alice.Listen()
}
