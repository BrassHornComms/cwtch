package main

import (
	"cwtch.im/cwtch/peer"
	"log"
	"strconv"
	"time"
)

func main() {
	bob, _ := peer.NewCwtchPeer("bob", "password", ".")
	counter := 1

	bob.SetPeerDataHandler(func(onion string, data []byte) []byte {
		log.Printf("Recieved %s from %v", data, onion)
		counter++
		return []byte(strconv.Itoa(counter))
	})
	connection := bob.PeerWithOnion("f4b6thuwmfszsqd3fzqpr45sdem4qoazdlzr2xmnc7fq22qe746hjqqd")

	log.Printf("Waiting for Bob to Connect to Alice...")
	connection.SendPacket([]byte("Hello Alice!!!"))

	// Wait a while...
	time.Sleep(time.Second * 100)

}
