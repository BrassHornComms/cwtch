package spam

import (
	"cwtch.im/cwtch/protocol"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"github.com/golang/protobuf/proto"
	"testing"
)

func TestSpamGuard(t *testing.T) {
	var spamGuard Guard
	spamGuard.Difficulty = 2
	challenge := spamGuard.GenerateChallenge(3)

	control := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(challenge[:], control)

	if control.GetChannelResult() != nil {
		ce, _ := proto.GetExtension(control.GetChannelResult(), protocol.E_ServerNonce)
		challenge := ce.([]byte)[:]

		sgsolve := spamGuard.SolveChallenge(challenge, []byte("Hello"))
		t.Logf("Solved: %v %v", challenge, sgsolve)
		result := spamGuard.ValidateChallenge([]byte("Hello"), sgsolve)
		if result != true {
			t.Errorf("Validating Guard Failed")
		}
		return
	}

	t.Errorf("Failed SpamGaurd")

}

func TestSpamGuardBadLength(t *testing.T) {
	var spamGuard Guard
	spamGuard.Difficulty = 2
	spamGuard.GenerateChallenge(3)
	result := spamGuard.ValidateChallenge([]byte("test"), []byte{0x00, 0x00})
	if result {
		t.Errorf("Validating Guard should have failed")
	}
}

func TestSpamGuardFail(t *testing.T) {
	var spamGuard Guard
	spamGuard.Difficulty = 2
	challenge := spamGuard.GenerateChallenge(3)

	control := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(challenge[:], control)

	if control.GetChannelResult() != nil {
		ce, _ := proto.GetExtension(control.GetChannelResult(), protocol.E_ServerNonce)
		challenge := ce.([]byte)[:]

		var spamGuard2 Guard
		spamGuard2.Difficulty = 1
		sgsolve := spamGuard2.SolveChallenge(challenge, []byte("Hello"))
		t.Logf("Solved: %v %v", challenge, sgsolve)
		result := spamGuard.ValidateChallenge([]byte("Hello"), sgsolve)
		if result {
			t.Errorf("Validating Guard successes")
		}
		return
	}

	t.Errorf("Failed SpamGaurd")
}
