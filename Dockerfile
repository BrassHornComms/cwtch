FROM golang as server-build-stage
ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/cwtch.im/cwtch
COPY . .

RUN go get -d -v ./...
#RUN go install -v ./...
WORKDIR /go/src/cwtch.im/cwtch/server/app/
RUN go build -ldflags "-extldflags '-static'"



#----------------------------------------------
FROM alpine:latest as tor-build-stage

# Install prerequisites
RUN apk --no-cache add --update \
        gnupg \
        build-base \
        libevent \
        libevent-dev \
        libressl \
        libressl-dev \
        xz-libs \
        xz-dev \
        zlib \
        zlib-dev \
        zstd \
        zstd-dev \
      #&& wget -q https://www.torproject.org/dist/tor-0.3.4.8.tar.gz \
      && wget -q https://www.torproject.org/dist/tor-0.3.5.2-alpha.tar.gz \
      && tar xf tor-0.3.5.2-alpha.tar.gz \
      && cd tor-0.3.5.2-alpha \
      && ./configure \
      && make install \
      && ls -R /usr/local/

FROM alpine:latest
MAINTAINER Ablative Hosting <support@ablative.hosting>

#BSD habits die hard
ENV TOR_USER=_tor

# Installing dependencies of Tor and pwgen
RUN apk --no-cache add --update \
      libevent \
      libressl \
      xz-libs \
      zlib \
      zstd \
      zstd-dev \
      pwgen

# Copy Tor
COPY --from=tor-build-stage /usr/local/ /usr/local/

# Create an unprivileged tor user
#RUN addgroup -g 19001 -S $TOR_USER && adduser -u 19001 -G $TOR_USER -S $TOR_USER
RUN addgroup -S $TOR_USER && adduser -G $TOR_USER -S $TOR_USER && adduser -G _tor -S cwtchd 

# Copy Tor configuration file
COPY ./server/docker/torrc /etc/tor/torrc

# Copy docker-entrypoint
COPY ./server/docker/docker-entrypoint /usr/local/bin/

# Copy across cwtch
COPY  --from=server-build-stage /go/src/cwtch.im/cwtch/server/app/app /usr/local/bin/cwtch_server

# Persist data
VOLUME /etc/tor /var/lib/tor

ENTRYPOINT ["docker-entrypoint"]

#cwtchd is in the _tor group so can access the socket but that's it
#USER cwtchd

#Launches the cwtchd daemon
CMD ["/usr/local/bin/cwtch_server"]
